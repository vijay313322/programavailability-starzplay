package com.starzplay.transformer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.starzplay.utils.JsonUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.starzplay.utils.Constants.*;

/**
 * Transformer class for response string.
 *
 * @author vijaykumar 7/8/2017
 */
@Component
public class ProgramAvailabilityResponseTransformer {

    /**
     * This method transforms the json string to list of jsonnodes based on condition.
     *
     * @param level
     * @param mediaContent
     * @return List<JsonNode>
     */
    public Optional<List<JsonNode>> transformResponse(final String level, final Optional<String> mediaContent) {

        if (mediaContent.isPresent()) {
            final JsonNode entriesNode = JsonUtils.jsonStringToNode(mediaContent.get()).get(FIELD_ENTRIES);
            final List<JsonNode> censoredJsonNode = new ArrayList<>();
            final List<JsonNode> mediaJsonNodeList = new ArrayList<>();

            entriesNode.forEach(entryJsonNode -> {
                if (CENSORED.equalsIgnoreCase(entryJsonNode.get(FIELD_CONTENT_CLASSIFICATION).textValue())) {
                    entryJsonNode.get(FIELD_MEDIA).forEach(
                        mediaJsonNode -> {
                            if (CENSORED.equalsIgnoreCase(level)) {
                                if (mediaJsonNode.get(FIELD_GUID).textValue().endsWith("C")) {
                                    mediaJsonNodeList.add(mediaJsonNode);
                                }
                            } else if (UNCENSORED.equalsIgnoreCase(level)) {
                                if (!mediaJsonNode.get(FIELD_GUID).textValue().endsWith("C")) {
                                    mediaJsonNodeList.add(mediaJsonNode);
                                }
                            }
                        }
                    );
                    censoredJsonNode.add(removeAndAddMediaNode(entryJsonNode, mediaJsonNodeList));
                }
            });
            return Optional.ofNullable(censoredJsonNode);
        }
        return Optional.empty();
    }

    private ObjectNode removeAndAddMediaNode(final JsonNode entryJsonNode, final List<JsonNode> mediaJsonNodeList) {
        final ObjectMapper mapper = new ObjectMapper();
        final ArrayNode array = mapper.valueToTree(mediaJsonNodeList);
        final ObjectNode mediaNode = mapper.valueToTree(entryJsonNode);
        mediaNode.putArray(FIELD_MEDIA).addAll(array);
        return mediaNode;
    }

}
