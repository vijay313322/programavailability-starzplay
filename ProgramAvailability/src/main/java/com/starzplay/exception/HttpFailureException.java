package com.starzplay.exception;

/**
 * Exception class for HTTP communication.
 *
 * @author vkumar 8/8/2017
 */
public class HttpFailureException extends RuntimeException {

    private static final long serialVersionUID = 693797988111878890L;

    public HttpFailureException(final String message) {
        super(message);
    }
}
