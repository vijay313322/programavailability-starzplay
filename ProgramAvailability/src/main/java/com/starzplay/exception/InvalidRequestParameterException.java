package com.starzplay.exception;

/**
 * Exception class for request paramater validation
 *
 * @author vkumar 8/8/2017
 */
public class InvalidRequestParameterException extends RuntimeException {

    private static final long serialVersionUID = 693797988111878890L;

    public InvalidRequestParameterException(final String message) {
        super(message);
    }

}
