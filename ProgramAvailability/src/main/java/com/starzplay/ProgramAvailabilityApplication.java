package com.starzplay;

import com.starzplay.config.StarzPlayConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@SpringBootApplication
@EnableAutoConfiguration
@Import(StarzPlayConfig.class)
public class ProgramAvailabilityApplication {

	public static void main(final String[] args) {
		SpringApplication.run(ProgramAvailabilityApplication.class, args);
	}
}
