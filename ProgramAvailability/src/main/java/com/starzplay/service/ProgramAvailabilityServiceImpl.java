package com.starzplay.service;

import com.starzplay.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import static com.starzplay.utils.Constants.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation for communicating to the http url.
 *
 * @author vijaykumar 7/8/2017
 */
@Service
public class ProgramAvailabilityServiceImpl implements ProgramAvailabilityService {

    private static final Logger logger = LoggerFactory.getLogger(ProgramAvailabilityServiceImpl.class);
    private static final Pattern DRIVE_URL_PATTERN = Pattern.compile(DRIVE_URL);

    @Value("${media.content.url}")
    private String mediaContentURL;

    /**
     * This method hits the drive url and gets the document response.
     *
     * @param filter
     * @param level
     * @return String
     */
    @Override
    public String getMediaContent(final String filter, final String level) {
        logger.info("Connecting to the content URL : {}", mediaContentURL);
        final String mediaPageContent = HttpUtils.executeGetUrl(mediaContentURL);
        logger.info("Response from the media content URL : {}", mediaPageContent);
        return HttpUtils.executeGetUrl(formDocumentURL(mediaPageContent));
    }


    private String documentID(final String webPageContent) {
        final Matcher matcher = DRIVE_URL_PATTERN.matcher(webPageContent);
        if (matcher.find()) {
            logger.info("Document ID : {}", matcher.group(1));
            return matcher.group(1);
        }
        return null;
    }

    private String formDocumentURL(final String webPageContent) {
        return DOC_URL + documentID(webPageContent);
    }
}
