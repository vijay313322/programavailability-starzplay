package com.starzplay.service;

/**
 * Interface for ProgramAvailability
 *
 * @author vkumar 7/8/2017
 */
public interface ProgramAvailabilityService {

    String getMediaContent(final String filter, final String level);

}
