package com.starzplay.utils;

import com.starzplay.exception.HttpFailureException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Utility class for http communication.
 *
 * @author vijaykumar 07/08/2017.
 */
public class HttpUtils {

    private static final Logger logger = LoggerFactory.getLogger(HttpUtils.class);
    private static final CloseableHttpClient CLOSEABLE_HTTP_CLIENT = HttpClients.createDefault();

    /**
     * This connects to the http url and get the response string.
     *
     * @param httpUrl
     * @return String
     */
    public static String executeGetUrl(final String httpUrl) {
        logger.info("Connecting to the URL : {}", httpUrl);
        String responseString = "";
        try {
            final HttpGet httpGet = new HttpGet(httpUrl);
            final HttpResponse httpResponse = CLOSEABLE_HTTP_CLIENT.execute(httpGet);
            responseString = EntityUtils.toString(httpResponse.getEntity());
        } catch (final IOException clientProtocolException) {
            logger.error(
                "An exception occurred : {} while connecting to the URL: {} : ",
                clientProtocolException, httpUrl);
            throw new HttpFailureException("An exception occurred during http call" + clientProtocolException);
        }
        return responseString;
    }

    private HttpUtils() {
    } // Hide utility class constructor
}
