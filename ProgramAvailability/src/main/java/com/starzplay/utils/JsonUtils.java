package com.starzplay.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Utility for Json Parsing.
 *
 * @author vkumar 7/8/2017
 */

public final class JsonUtils {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * This method converts the json string to json node.
     *
     * @param jsonString
     * @return JsonNode
     */
    public static JsonNode jsonStringToNode(final String jsonString) {
        try {
            return MAPPER.readTree(jsonString);
        } catch (final IOException cause) {
            final String jsonStringStart64 = (64 < jsonString.length() ? jsonString.substring(0, 64) : jsonString);
            throw new IllegalArgumentException("Unable to parse JSON string: " + jsonStringStart64, cause);
        }
    }

    // Hide utility class constructor
    private JsonUtils() {
    }
}
