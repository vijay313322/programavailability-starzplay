package com.starzplay.utils;

import java.util.regex.Pattern;

/**
 * @author vijaykumar 7/8/2017
 */
public class Constants {

    public static final String DOC_URL = "https://docs.google.com/uc?export=download&id=";
    public static final String DRIVE_URL = "https://drive.google.com/file/d/(.*?)/";

    public static final String FIELD_MEDIA = "media";
    public static final String FIELD_GUID = "guid";
    public static final String FIELD_CONTENT_CLASSIFICATION = "peg$contentClassification";
    public static final String CENSORED = "Censored";
    public static final String UNCENSORED = "Uncensored";
    public static final String FIELD_ENTRIES = "entries";
}
