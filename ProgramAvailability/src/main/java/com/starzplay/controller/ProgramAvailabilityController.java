package com.starzplay.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.starzplay.processor.ProgramAvailabilityProcessor;
import com.starzplay.validator.ProgramAvailabilityRequestValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;


/**
 * Controller class for ProgramAvailability Service.
 *
 * @author vijaykumar 8/8/2017
 */

@RestController
public class ProgramAvailabilityController {

    private static final Logger logger = LoggerFactory.getLogger(ProgramAvailabilityController.class);

    @Autowired
    private ProgramAvailabilityProcessor programAvailabilityProcessor;

    @Autowired
    private ProgramAvailabilityRequestValidator programAvailabilityRequestValidator;

    @Value("#{'${valid.filters}'.split(',')}")
    private Set<String> validFilters;

    @RequestMapping(
        produces = MediaType.APPLICATION_JSON_VALUE,
        value = "/media",
        method = RequestMethod.GET)
    public List<JsonNode> getProgramAvailabilty(@RequestParam final String filter, @RequestParam final String level) {
        logger.info("Query Params - Filter : {} and Level : {}", filter, level);
        programAvailabilityRequestValidator.validateRequest(filter, level, validFilters);
        return programAvailabilityProcessor.process(filter, level);
    }
}
