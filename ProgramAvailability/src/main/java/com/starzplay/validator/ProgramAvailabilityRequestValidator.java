package com.starzplay.validator;

import com.starzplay.exception.InvalidRequestParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Validation for the request parameters.
 *
 * @author vkumar 3/1/2017
 */

@Component
public class ProgramAvailabilityRequestValidator {

    private static final Logger LOGGER;

    static {
        LOGGER = LoggerFactory.getLogger(ProgramAvailabilityRequestValidator.class);
    }

    private static final Set<String> PROGRAMAVAILABILITYLEVELS = new HashSet<>(Arrays.asList("uncensored", "censored"));

    public void validateRequest(final String filter, final String level, final Set<String> validFiltersList) {
        if (!validFiltersList.contains(filter)) {
            LOGGER.error("Invalid query paramter : filter!!!");
            throw new InvalidRequestParameterException("Invalid query paramter : filter!!!");
        }
        if (!PROGRAMAVAILABILITYLEVELS.contains(level)) {
            LOGGER.error("Invalid query paramter : level!!!");
            throw new InvalidRequestParameterException("Invalid query paramter : level!!!");
        }
    }
}
