package com.starzplay.processor;

import com.fasterxml.jackson.databind.JsonNode;
import com.starzplay.service.ProgramAvailabilityService;
import com.starzplay.transformer.ProgramAvailabilityResponseTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Processor class for the incoming request and return the json nodes.
 *
 * @author vkumar 7/8/2017
 */

@Component
public class ProgramAvailabilityProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ProgramAvailabilityProcessor.class);

    @Autowired
    private ProgramAvailabilityService programAvailabilityServiceImpl;

    @Autowired
    private ProgramAvailabilityResponseTransformer programAvailabilityResponseTransformer;

    public List<JsonNode> process(final String filter, final String level) {
        logger.info("Processing the request...");
        final Optional<String> mediaContent = Optional.ofNullable(programAvailabilityServiceImpl.getMediaContent(
            filter,
            level));
        final List<JsonNode> programJsonNodeList =
            programAvailabilityResponseTransformer.transformResponse(level, mediaContent).get();
        logger.info("Processed the request.");
        return programJsonNodeList;
    }

}
