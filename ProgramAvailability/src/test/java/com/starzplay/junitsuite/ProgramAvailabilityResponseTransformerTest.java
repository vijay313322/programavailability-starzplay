package com.starzplay.junitsuite;

import com.fasterxml.jackson.databind.JsonNode;
import com.starzplay.transformer.ProgramAvailabilityResponseTransformer;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Test class for Program Availability Response Transformer.
 *
 * @author vkumar 8/8/2017
 */
public class ProgramAvailabilityResponseTransformerTest {

    private ProgramAvailabilityResponseTransformer programAvailabilityResponseTransformer;

    @Before
    public void setUp() {
        programAvailabilityResponseTransformer = new ProgramAvailabilityResponseTransformer();
    }

    @Test
    public void testCensoredMedia() throws IOException {
        final String mediaContentFile = "testdata/media-content.json";
        final InputStream mediaContentStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
            mediaContentFile);
        if (mediaContentFile == null) {
            fail("Sample media content file '" + mediaContentFile + "' could not be loaded.");
        }
        final Optional<String> mediaContent = Optional.ofNullable(IOUtils.toString(mediaContentStream));
        final List<JsonNode> jsonNodeList = programAvailabilityResponseTransformer.transformResponse(
            "Censored",
            mediaContent).get();
        assertFields(jsonNodeList, true);
    }

    @Test
    public void testUncensoredMedia() throws IOException {
        final String mediaContentFile = "testdata/media-content.json";
        final InputStream mediaContentStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(
            mediaContentFile);
        if (mediaContentFile == null) {
            fail("Sample media content file '" + mediaContentFile + "' could not be loaded.");
        }
        final Optional<String> mediaContent = Optional.ofNullable(IOUtils.toString(mediaContentStream));
        final List<JsonNode> jsonNodeList = programAvailabilityResponseTransformer.transformResponse(
            "Uncensored",
            mediaContent).get();
        assertFields(jsonNodeList, false);
    }

    private void assertFields(final List<JsonNode> jsonNodeList, final boolean assertTrueOrFalse) {
        assertNotNull(jsonNodeList);
        jsonNodeList.forEach(
            jsonNode -> {
                assertEquals("Censored", jsonNode.get("peg$contentClassification").textValue());
                jsonNode.get("media").forEach(
                    mediaNode -> {
                        if (assertTrueOrFalse) {
                            assertTrue(mediaNode.get("guid").textValue().endsWith("C"));
                        } else {
                            assertTrue(!mediaNode.get("guid").textValue().endsWith("C"));
                        }
                    }
                );
            }
        );
    }

}
