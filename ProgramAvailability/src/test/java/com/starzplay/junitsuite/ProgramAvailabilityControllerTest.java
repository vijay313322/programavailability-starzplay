package com.starzplay.junitsuite;

import com.fasterxml.jackson.databind.JsonNode;
import com.starzplay.controller.ProgramAvailabilityController;
import com.starzplay.exception.InvalidRequestParameterException;
import com.starzplay.processor.ProgramAvailabilityProcessor;
import com.starzplay.service.ProgramAvailabilityService;
import com.starzplay.transformer.ProgramAvailabilityResponseTransformer;
import com.starzplay.validator.ProgramAvailabilityRequestValidator;
import org.apache.commons.io.IOUtils;
import org.easymock.EasyMock;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertNotNull;

/**
 * The junit test cases for {@link ProgramAvailabilityController} class.
 *
 * @author vkumar 8/8/2017
 */
public class ProgramAvailabilityControllerTest {

    private final ProgramAvailabilityController programAvailabilityController = new ProgramAvailabilityController();
    private final ProgramAvailabilityProcessor programAvailabilityProcessor = new ProgramAvailabilityProcessor();
    private final ProgramAvailabilityRequestValidator programAvailabilityRequestValidator =
        new ProgramAvailabilityRequestValidator();
    private final ProgramAvailabilityService programAvailabilityServiceImpl =
        EasyMock.createMock(ProgramAvailabilityService.class);
    private final ProgramAvailabilityResponseTransformer programAvailabilityResponseTransformer =
        new ProgramAvailabilityResponseTransformer();

    @Test
    public void testSuccessfulResponse() throws IOException {

        final Set<String> validFilters = new HashSet<>(Collections.singletonList("censoring"));
        setupMocks(validFilters);

        final List<JsonNode> responseNode = programAvailabilityController.getProgramAvailabilty(
            "censoring",
            "censored");

        assertNotNull(responseNode);
    }

    @Test(expected = InvalidRequestParameterException.class)
    public void testInvalidParameterForInvalidFilter() throws IOException {

        final Set<String> validFilters = new HashSet<>(Collections.singletonList("Uncensoring"));
        setupMocks(validFilters);

        final List<JsonNode> responseNode = programAvailabilityController.getProgramAvailabilty(
            "censoring",
            "censored");
    }

    @Test(expected = InvalidRequestParameterException.class)
    public void testInvalidParameterForInvalidLevel() throws IOException {

        final Set<String> validFilters = new HashSet<>(Collections.singletonList("Uncensoring"));
        setupMocks(validFilters);

        final List<JsonNode> responseNode = programAvailabilityController.getProgramAvailabilty(
            "censoring",
            "test");
    }

    public void setupMocks(final Set<String> validFilters) throws IOException {
        ReflectionTestUtils.setField(
            programAvailabilityController,
            "validFilters",
            validFilters);

        ReflectionTestUtils.setField(
            programAvailabilityProcessor,
            "programAvailabilityResponseTransformer",
            programAvailabilityResponseTransformer);

        final InputStream mediaContentStream =
            this.getClass().getClassLoader().getResourceAsStream("testdata/media-content.json");
        final String mediaContentText = IOUtils.toString(mediaContentStream);

        EasyMock.expect(programAvailabilityServiceImpl.getMediaContent(
            EasyMock.isA(String.class),
            EasyMock.isA(String.class)))
            .andReturn(mediaContentText).anyTimes();

        ReflectionTestUtils.setField(
            programAvailabilityProcessor,
            "programAvailabilityServiceImpl",
            programAvailabilityServiceImpl);

        EasyMock.replay(programAvailabilityServiceImpl);

        ReflectionTestUtils.setField(
            programAvailabilityController,
            "programAvailabilityRequestValidator",
            programAvailabilityRequestValidator);

        ReflectionTestUtils.setField(
            programAvailabilityController,
            "programAvailabilityProcessor",
            programAvailabilityProcessor);

    }

}
