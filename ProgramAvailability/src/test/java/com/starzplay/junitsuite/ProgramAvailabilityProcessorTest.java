package com.starzplay.junitsuite;

import com.fasterxml.jackson.databind.JsonNode;
import com.starzplay.processor.ProgramAvailabilityProcessor;
import com.starzplay.service.ProgramAvailabilityService;
import com.starzplay.transformer.ProgramAvailabilityResponseTransformer;
import org.apache.commons.io.IOUtils;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test case for ProgramAvailabilityProcessor.
 *
 * @author vkumar 8/8/2017
 */
public class ProgramAvailabilityProcessorTest {

    private ProgramAvailabilityProcessor programAvailabilityProcessor;
    private final ProgramAvailabilityService programAvailabilityServiceImpl =
        EasyMock.createMock(ProgramAvailabilityService.class);
    private final ProgramAvailabilityResponseTransformer programAvailabilityResponseTransformer =
        new ProgramAvailabilityResponseTransformer();

    @Before
    public final void setUp() {
        programAvailabilityProcessor = new ProgramAvailabilityProcessor();
        ReflectionTestUtils.setField(
            programAvailabilityProcessor,
            "programAvailabilityServiceImpl",
            programAvailabilityServiceImpl);
        ReflectionTestUtils.setField(
            programAvailabilityProcessor,
            "programAvailabilityResponseTransformer",
            programAvailabilityResponseTransformer);
    }

    @Test
    public void processCensoredTest() throws IOException {

        final InputStream mediaContentStream =
            this.getClass().getClassLoader().getResourceAsStream("testdata/media-content.json");
        final String mediaContentText = IOUtils.toString(mediaContentStream);

        EasyMock.expect(programAvailabilityServiceImpl.getMediaContent(
            EasyMock.isA(String.class),
            EasyMock.isA(String.class))).andReturn(mediaContentText).anyTimes();
        EasyMock.replay(programAvailabilityServiceImpl);
        EasyMock.verify(programAvailabilityServiceImpl);

        final List<JsonNode> responseJsonList = programAvailabilityProcessor.process("censoring", "censored");
        assertFields(responseJsonList, true);
    }

    @Test
    public void processUnCensoredTest() throws IOException {

        final InputStream mediaContentStream =
            this.getClass().getClassLoader().getResourceAsStream("testdata/media-content.json");
        final String mediaContentText = IOUtils.toString(mediaContentStream);

        EasyMock.expect(programAvailabilityServiceImpl.getMediaContent(
            EasyMock.isA(String.class),
            EasyMock.isA(String.class))).andReturn(mediaContentText).anyTimes();
        EasyMock.replay(programAvailabilityServiceImpl);
        EasyMock.verify(programAvailabilityServiceImpl);

        final List<JsonNode> responseJsonList = programAvailabilityProcessor.process("censoring", "Uncensored");
        assertFields(responseJsonList, false);
    }

    private void assertFields(final List<JsonNode> jsonNodeList, final boolean assertTrueOrFalse) {
        assertNotNull(jsonNodeList);
        jsonNodeList.forEach(
            jsonNode -> {
                assertEquals("Censored", jsonNode.get("peg$contentClassification").textValue());
                jsonNode.get("media").forEach(
                    mediaNode -> {
                        if (assertTrueOrFalse) {
                            assertTrue(mediaNode.get("guid").textValue().endsWith("C"));
                        } else {
                            assertTrue(!mediaNode.get("guid").textValue().endsWith("C"));
                        }
                    }
                );
            }
        );
    }

}
