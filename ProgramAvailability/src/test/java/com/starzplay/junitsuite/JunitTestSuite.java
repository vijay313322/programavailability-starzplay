package com.starzplay.junitsuite;

/**
 * @author vijaykumar 08/08/2017
 */

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
    ProgramAvailabilityControllerTest.class,
    ProgramAvailabilityResponseTransformerTest.class,
    ProgramAvailabilityProcessorTest.class
})

public class JunitTestSuite {
}
